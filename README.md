# project for WAP by Marek Timm and Kamil Kóska

Back-end: java with servlets, REST
Front-end: React - https://gitlab.com/marektimm33/web-application-project-frontend
To use it download from site, use "npm i" in cmd when inside downloaded folder to download required modules, then "npm start" to initialize front-end.
The mockup is available on the https://xd.adobe.com/view/1c6e4c65-7d33-4fa6-afdf-5ba47aa6cff7-493d/grid/?fbclid=IwAR0bz8euwaKIKJvsJ0S3df3mrmaWWf3aWeTPP9L0CtjAqVBdPZc4bkcfqEU
