import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import Editable from "../_components/Editable";

import { accountService, alertService } from '@/_services';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete';
import CheckIcon from '@material-ui/icons/Check';
import CloseIcon from '@material-ui/icons/Close';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Rating from '@material-ui/lab/Rating';
import Box from '@material-ui/core/Box';
import './profileStyles.less';

function Details({ match }) {
    const { path } = match;
    
    
    let user = accountService.userValue;
    const [tags, setTags] = useState([]);
    const [newTag, setNewTag] = useState('');
    const [newTagWeight, setNewTagWeight] = useState(1);
    const [allTags, setAllTags] = useState([]);
    const [isAddTagVisible, setIsAddTagVisible] = useState(false);
    useEffect(() => {
        accountService.getTags({user_id: user.user_id}).then(x => setTags(x));
        }, []);
    useEffect(() => {
        accountService.getAllTags().then(x => x.filter(tag => !tags.find(y => y.tag_id == tag.tag_id))).then(x => setAllTags(x));
    }, [tags]);
    useEffect(() => {
        allTags.length && setNewTag(allTags[0].tag_id);
    }, [allTags]);
    const handleChange = (event) => {
        setNewTag(event.target.value);
      };
    
    const [description, setDescription] = useState(user.description ? user.description : "Nie masz jeszcze żadnego opisu. Kliknij aby zmienić");

 
    const ChangeDescription = (description, user) => {
        accountService.updateDescription({user_id: user.user_id, description: description});
        user.description = description;
        userSubject.next(user);
    }


    const AddTag = () => {
        const payload = {
            user_id: user.user_id,
            tag_id: newTag,
            weight: newTagWeight
        }
        accountService.addTag(payload)
            .then(() => {
                alertService.success('Dodano tag');
                const tempTags = tags;
                let addedTag = allTags.find(x => x.tag_id == newTag);
                addedTag.weight = newTagWeight;
                tempTags.push(addedTag);
                const newAllTags = allTags.filter(x => x.tag_id != newTag);
                setTags(tempTags);
                setAllTags(newAllTags);
                setNewTagWeight(1);
                setIsAddTagVisible(false);
            })
            .catch(error => {
                alertService.error(error);
            });
    }

    const DeleteTag = (id) => {
        const payload = {
            user_id: user.user_id,
            tag_id: id,
        }
        accountService.deleteTag(payload)
            .then(() => {
                alertService.success('Usunięto tag');
                setTags(tags.filter(tag => tag.tag_id != id));
            })
            .catch(error => {
                alertService.error(error);
            });

    }

    return (
        <div className="profile-details">
             <Grid container spacing={6} justify="space-between">
                <Grid item md={8}>
                    <h1>Ustawienia profilu</h1>
                    <Grid container spacing={10} justify="space-between">
                        <Grid item md={4}>
                            <h2>Ogólne</h2>   
                            <TextField
                                disabled
                                id="name"
                                label="Nazwa"
                                defaultValue={user.name}
                                variant="filled"
                                className="profile-textfield"
                            />
                            <TextField
                                disabled
                                id="email"
                                label="Email"
                                defaultValue={user.email}
                                variant="filled"
                                className="profile-textfield"
                            />
                        </Grid>
                        <Grid item md={8}>
                            <h2>Tagi</h2>   
                            {tags.length? 
                                tags.map( tag => 
                                <Grid key={tag.tag_id}  container alignItems="center" justify="space-between" className="tag-list-element">
                                    <Grid item xs={6}>
                                        <p>
                                            {tag.name}
                                        </p> 
                                    </Grid >
                                    <Grid item xs={4}>
                                        <Box component="fieldset" mb={3} borderColor="transparent">
                                            <Rating name={tag.name} value={tag.weight} disabled max={3} />
                                        </Box>
                                    </Grid >
                                    <Grid item xs={2} >
                                        <DeleteIcon onClick={() => DeleteTag(tag.tag_id)}></DeleteIcon>
                                    </Grid >
                                </Grid>
                                )
                                :
                                <h3>Nie masz jeszcze żadnych tagów</h3>
                            } 
                            {!!allTags.length && <>{   
                                !isAddTagVisible ? 
                                <p className="profile-textfield add-button" onClick={() => setIsAddTagVisible(true)}>
                                    <AddIcon></AddIcon> Dodaj nowy tag
                                </p>
                                :
                                <Grid container alignItems="center" justify="space-between" className="tag-list-element">
                                    <Grid item xs={5}>
                                        <FormControl>
                                            <Select
                                            value={newTag}
                                            onChange={handleChange}
                                            >
                                                {allTags.map(tag =>
                                                    <MenuItem key={tag.tag_id} value={tag.tag_id}>{tag.name}</MenuItem>
                                                    )}
                                            </Select>
                                            <FormHelperText>Select new tag</FormHelperText>
                                        </FormControl> 
                                    </Grid >
                                    <Grid item xs={3}>
                                        <Box component="fieldset" mb={3} borderColor="transparent">
                                            <Rating
                                            name="newTagWeight"
                                            value={newTagWeight}
                                            onChange={(event, newValue) => {
                                                setNewTagWeight(newValue);
                                            }}
                                            max={3}
                                            />
                                        </Box>
                                    </Grid >
                                    <Grid item xs={3} >
                                        <CheckIcon onClick={() => AddTag()}></CheckIcon>
                                        <CloseIcon onClick={() => setIsAddTagVisible(false)}></CloseIcon>
                                    </Grid >
                                </Grid>
                            }</>}
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={4}>
                    <div className="side-panel-container">
                        <img src="src\resources\6-avatar.jpeg" className="user-avatar"></img><br/>
                        <h1>{user.name}</h1>
                        <br/>
                        <h3>Opis </h3>
                        <Editable
                        text={description}
                        type="input"
                        >
                        <textarea
                            
                            name="description"
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            onBlur={() => ChangeDescription(description, user)}
                        />
                        </Editable> 
                    </div>
                </Grid>
            </Grid>
            {/* <p><Link to={`${path}/update`}>Aktualizuj profil</Link></p> */}
        </div>
    );


   
}
export { Details };