import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { Details } from './Details';
import { Update } from './Update';
import './profileStyles.less';

function Profile({ match }) {
    const { path } = match;
    
    return (
        <div className="profile">
                <Switch>
                    <Route exact path={path} component={Details} />
                    {/* <Route path={`${path}/update`} component={Update} /> */}
                </Switch>
        </div>
    );
}

export { Profile };