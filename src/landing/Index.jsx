import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import './landingStyle.less'

const LandingPage = () => (
    <div className='landing-wrapper'>
       <div id="first-block">
            <LearnMore/>
            <MainBlock/>
        </div>
       <AboutRPG/>
       <AboutApp/>
       <YouDecide/>
       <LastBlock/>
    </div>
);

const MainBlock = () => (
    <div  id="main-block">
        <div className="landing-block right">
            <h1> Twoje miejsce spotkań</h1>
            <p>
                Lubisz RPG ale nie masz z kim grać? <br/>
                Chcesz spróbować ale brakuje Ci towarzystwa? <br/>
                MeetRPG pomoże Ci znaleźć wymarzoną drużynę!
            </p>
            </div>
    </div>
);
const LearnMore = () => (
    <div  id="learn-more">
        <div className="landing-block left">
            <p>
                Wciąż nieprzekonany? <br/>
                Nie wiesz o czym mowa?
            </p>
            <Button className="landing-button" variant="contained" color="secondary">Dowiedz się więcej</Button>
        
        </div>
    </div>
);
const AboutRPG = () => (
    <div id="about-rpg">
        <div className="landing-block right">
            <h1> Wspólne snucie opowieści</h1>
            <p>
                RPG to skrót od angielskiego role-playing games, czyli dosłownie „gry w odgrywanie”. 
            </p>
            <p>
                Mistrz Gry opisuje, co dzieje się dookoła postaci graczy, a gracze wcielają się w swoje postacie, opowiadając, gestykulując i rozmawiając ze sobą. 
            </p>
            <p>
                Dzięki MeetRPG możesz wygodnie znajdować graczy z Twojej okolicy lub z zupełnie odległych miejsc!
            </p>
        </div>
    </div>
);
const AboutApp = () => (
    <div id="about-app">
        <div className="landing-block left">
            <h1> Sam ustalasz zasady</h1>
            <p>
                W MeetRPG jako Mistrz Gry tworzysz pokoje, w których to Ty ustalasz w co i kiedy gracie. Możesz też określić ilu śmiałków potrzebujesz by gra się odbyła. 
            </p>
            <p>
                Dzięki wygodnym tagom Twoi przyszli gracze łatwo Cię odnajdą! 
            </p>
        </div>
    </div>
);
const YouDecide = () => (
    <div id="you-decide">
        <div className="landing-block right">
            <h1> Ty decydujesz</h1>
            <p>
            Jako gracz możesz łatwo i szybko znaleźć pokój z systemem który Cię interesuje. Pokoje do których dołączysz będą zawsze na Ciebie czekać! 
            </p>
            <p>
                Pamiętaj, że możesz być graczem i Mistrzem Gry jednocześnie! 
            </p>
        </div>
    </div>
);
const LastBlock = () => (
    <div id="last-block">
        <div className="center-block">
            <h1> Gotowy do rozpoczęcia przygody?</h1>
            <NavLink to="/account/register"><Button className="landing-button" variant="contained" color="primary">Dołącz do meetrpg</Button></NavLink>
        </div>
    </div>
);



export { LandingPage }; 