import React from 'react';
import { Router } from 'react-router-dom';
import { render } from 'react-dom';

import { history } from './_helpers';
import { accountService } from './_services';
import { App } from './app';
import { createMuiTheme , ThemeProvider } from '@material-ui/core/styles';
import moment from 'moment';

import './styles.less';

// setup fake backend

// attempt silent token refresh before startup
moment.locale('pl');
const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#fff',
        contrastText: '#410865',
      },
      secondary: {
        main: '#CF15A9',
        contrastText: '#fff',
      },      
    },
  });
startApp();
function startApp() { 
    render(
        <React.StrictMode>
            <ThemeProvider theme={theme}>
                <Router history={history}>
                    <App />
                </Router>
            </ThemeProvider>
        </React.StrictMode>,
        document.getElementById('app')
    );
}