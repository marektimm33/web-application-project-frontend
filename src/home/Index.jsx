import React, { useState, useEffect } from 'react';

import { NavLink, Route, useLocation  } from 'react-router-dom';
import { accountService, tableService } from '@/_services';
import Button from '@material-ui/core/Button';
import FilledInput from '@material-ui/core/FilledInput';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import SearchIcon from '@material-ui/icons/Search';
import moment from 'moment';
import ForumIcon from '@material-ui/icons/Forum';
import AddIcon from '@material-ui/icons/Add';


import Grid from '@material-ui/core/Grid';
import "./homeStyles.less"
const Friend = ({user}) => {
    const imgAdress = `src\\resources\\${user.avatar}.jpeg`;

    return (
        <div>
            <Grid container justify="space-between" alignItems="center" spacing={5}>
                <Grid item sm={3}>
                    <img src={imgAdress} className="friend-avatar"></img>
                </Grid>
                <Grid item sm={6}>
                    <h5>@{user.name}</h5>
                </Grid>
                <Grid item sm={3}>
                    <ForumIcon></ForumIcon>
                </Grid>
            </Grid>
        </div>
    )
}
const Friends = ({friends}) => {

    return (
        <div className="friends-container">
            <Grid container justify="space-between" alignItems="center">
                <Grid item sm={8}>
                    <h2>Znajomi</h2>
                </Grid>
                <Grid item sm={4}>
                    <h6><AddIcon></AddIcon>DODAJ</h6>
                </Grid>
            </Grid>
            {friends?.map(function(friend, index){
                    return <Friend key={ index } user={friend}></Friend>;
                  })}
        </div>
    )
}


const Home = () => {
    const [rooms, setTables] = useState([]);
    const user = accountService.userValue;
    const { search } = window.location;
    const query = new URLSearchParams(search).get('s');
    const [searchQuery, setSearchQuery] = useState(query || '');
    const filteredRooms = filterRooms(rooms, searchQuery);

    const [tags, setTags] = useState([]);
    useEffect(() => {
        accountService.getTags({user_id: user.user_id}).then(x => setTags(x));
        }, []);

    useEffect(() => {
        tableService.getAll().then(x => {
            let score;
            x.forEach(room => {
                score = 0;
                tags.forEach(tag => {
                    room.tags.find(roomTag => roomTag.tag_id == tag.tag_id) && (score += tag.weight);
                });
                room.score = score;
            });
            x.sort((a,b) => b.score - a.score);
            console.log(x)
            setTables(x);
        });

    }, [tags]);



    const friends = [
        {user_id: 1, name: "ktoś-nowy", avatar: "1-avatar"},
        {user_id: 2, name: "alien_love", avatar: "2-avatar"},
        {user_id: 3, name: "zena", avatar: "3-avatar"},
        {user_id: 4, name: "alien-alien", avatar: "4-avatar"},
    ]

    
    return (
        <div className="home-wrapper">
            <Grid container spacing={10} justify="center">
                <Grid item md={8}>
                    <FormControl fullWidth id="search-bar-container" variant="filled">
                        <FilledInput
                            id="search-bar"
                            value={searchQuery}
                            onInput={e => setSearchQuery(e.target.value)}

                            placeholder="Szukaj pokoju"
                            name="s"
                            startAdornment={<InputAdornment position="start"><SearchIcon /></InputAdornment>}
                        />
                    </FormControl>
                    {!rooms? 
                        <div>
                            <p>Nie znaleziono żadnych pokoi! Może chcesz stworzyć nowy?</p>
                            <Button className="landing-nav-button" variant="contained" color="primary">Dodaj pokój</Button>
                        </div> 
                    : 
                        <Grid container spacing={10} justify="space-between">
                            {filteredRooms.map(room => 
                                    <Grid item md={6} xl={4} key={room.room_id}>
                                        <CreateRoom room={room}/>
                                    </Grid>
                                ) }                   
                        </Grid>
                    }
                    {/* <div className="rooms-container">
                            <ListRooms rooms={rooms}/>
                    </div> */}
                </Grid>
                <Grid item md={4}>
                    <div className="side-panel-container">
                        <Friends friends={friends}></Friends>
                    </div>
                </Grid>
            </Grid>
        </div>
    );

}

const CreateRoom = ({room}) => {
    console.log(room)
    return (
        <div className="room">
            <p>Mistrzem gry jest: {room.users.find(user => user.user_id == room.owner_id).name}</p>
            <img src="src\resources\room-placeholder.jpg" className="room-image"></img>
            <div>
                {(room.hasOwnProperty('tags') && room.tags.length)?
                    <p>{room.tags.map( tag => `#${tag.name} `)}</p>
                :
                    <p>Nie ma tagów</p>
                }
                <p>Liczba miejsc: {room.users.length}/{room.number_of_slots}</p>
                <h4>{room.name}</h4>
                <p>{room.description}</p>
                {room.next_session?
                    <p>Najbliższe spotkanie {moment(room.next_session).fromNow()}</p>
                :
                    <p>Nie ma jeszcze zaplanowanego spotkania</p>
                }
                <NavLink to={`table/${room.room_id}`}><Button variant="contained" color="secondary">Szczegóły</Button></NavLink>
            </div> 
        </div>
    );
    
}

const filterRooms = (rooms, query) => {
    if (!query) {
        return rooms;
    }

    return rooms.filter((room) => {
        const roomName = room.name.toLowerCase();
        return roomName.includes(query);
    });
};

export { Home };