import React, { useState, useEffect } from 'react';
import { NavLink, Route, Link, useLocation } from 'react-router-dom';

import { Role } from '@/_helpers';
import { accountService } from '@/_services';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

import './componentsStyles.less';

function Footer() {
    const location = useLocation();
    if (location.pathname.split('/')[1] == "account") return null;
    return (
        <div>
            <div className="footer">
                <Grid id="footer-container" container spacing={3} justify="center" alignItems="center">
                    <Grid item md={3}>
                        <h1 className="logo">MEET RPG</h1>
                    </Grid>
                    <Grid item md={3}>
                        <span className="footer-link">Regulamin</span> 
                    </Grid>
                    <Grid item md={3}>
                        <span className="footer-link">Polityka prywatności</span>
                    </Grid>
                    <Grid item md={3}>
                        <span className="footer-link">Wsparcie</span>
                    </Grid>
                        
                </Grid>
            </div>
        </div>
    );
}


export { Footer }; 