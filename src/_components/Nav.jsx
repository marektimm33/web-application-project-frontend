import React, { useState, useEffect } from 'react';
import { NavLink, Route, useLocation  } from 'react-router-dom';

import { Role } from '@/_helpers';
import { accountService } from '@/_services';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import ForumIcon from '@material-ui/icons/Forum';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';

import './componentsStyles.less';

function Nav() {
    const [user, setUser] = useState({});

    useEffect(() => {
        const subscription = accountService.user.subscribe(x => setUser(x));
        return subscription.unsubscribe;
    }, []);
    const location = useLocation();
    // only show nav when logged in

    if (!user) return LandingNav(location);

    return (
        <div>
            <nav className="main-navbar">
                <Grid container spacing={3} justify="center" alignItems="center">
                    <Grid item md={3}>
                        <NavLink to="/home" ><h1 className="logo">MEET RPG</h1></NavLink>
                    </Grid>
                    <Grid item md={4}>
                    </Grid>
                    <Grid item md={1}>
                        <a className="landing-link"><ForumIcon style={{marginRight: '5px', marginBottom: '3px'}}></ForumIcon>Chat</a>
                    </Grid>
                    <Grid item md={1}>
                        <NavLink to="/profile" className="landing-link"><AccountCircleIcon style={{marginRight: '5px', marginBottom: '3px'}}></AccountCircleIcon>Profil</NavLink>
                    </Grid>
                    <Grid item md={1}>
                        <a onClick={accountService.logout} className="landing-link"><ExitToAppIcon style={{marginRight: '5px', marginBottom: '3px'}}></ExitToAppIcon>Wyloguj</a>
                    </Grid>
                    <Grid item md={2}>
                        <NavLink to="/table/create" className="landing-link"><Button className="landing-nav-button" variant="contained" color="primary">Dodaj pokój</Button></NavLink>
                    </Grid>
                        
                </Grid>
            </nav>
        </div>
    );
}

function LandingNav(location) {
    if (location.pathname != "/") return null;

    return (
        <div>
            <nav className="landing-navbar">
                <Grid container spacing={3} justify="center" alignItems="center">
                    <Grid item md={2}>
                        <h1 className="logo">MEET RPG</h1>
                    </Grid>
                    <Grid item md={2}>
                        <a href="#about-rpg" className="landing-link">Czym jest RPG?</a>
                    </Grid>
                    <Grid item md={2}>
                        <a href="#about-app" className="landing-link">Dlaczego Meet RPG?</a>
                    </Grid>
                    <Grid item md={2}>
                        <a href="#you-decide" className="landing-link">Wsparcie</a>
                    </Grid>
                    <Grid item md={2}>
                        <NavLink to="/account/register"><Button className="landing-nav-button" variant="contained" color="primary">Dołącz do MeetRPG</Button></NavLink>
                    </Grid>
                    <Grid item md={2}>
                        <NavLink to="/account/login"><Button className="landing-nav-button special-button" variant="contained">Zaloguj się</Button></NavLink>
                    </Grid>
                </Grid>
            </nav>
        </div>
    );
}

function AdminNav({ match }) {
    const { path } = match;

    return (
        <nav className="admin-nav navbar navbar-expand navbar-light">
            <div className="navbar-nav">
                <NavLink to={`${path}/users`} className="nav-item nav-link">Users</NavLink>
            </div>
        </nav>
    );
}

export { Nav }; 