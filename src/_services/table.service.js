import config from 'config';
import { fetchWrapper } from '@/_helpers';

const baseUrl = `${config.apiUrl}/Tables`;

export const tableService = {
    getAll,
    getById,
    create,
    update,
    joinRoom,
    leaveRoom,
    delete: _delete
};

function getAll() {
    return fetchWrapper.get(`${baseUrl}/all`);
}

function getById(id) {
    return fetchWrapper.get(`${baseUrl}/room/${id}`);
}

function create(params) {
    return fetchWrapper.post(`${baseUrl}/create`, params);
}

function joinRoom(params) {
    return fetchWrapper.post(`${baseUrl}/join`, params);
}

function leaveRoom(params) {
    return fetchWrapper.post(`${baseUrl}/leave`, params);
}

function update(id, params) {
    return fetchWrapper.put(`${baseUrl}/${id}`, params)
        .then(table => {
            return table;
        });
}

// prefixed with underscore because 'delete' is a reserved word in javascript
function _delete(id) {
    return fetchWrapper.delete(`${baseUrl}/${id}`)
        .then(x => {
            return x;
        });
}