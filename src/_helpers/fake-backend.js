import { Role } from './'
import { alertService } from '@/_services';

// array in local storage for registered users
const usersKey = 'react-signup-verification-boilerplate-users';
const tablesKey = 'react-signup-verification-boilerplate-tables';
let users = JSON.parse(localStorage.getItem(usersKey)) || [];
let tables = JSON.parse(localStorage.getItem(tablesKey)) || [
	{
		"room_id" : 1,
		"description" : "to jest pokoj A",
		"status" : "on",
		"avatar" : null,
		"RoomChat_chat_id" : 1,
		"Users_user_id" : 1,
		"owner_id" : 1,
		"name" : "Jak obrabować smoka",
		"next_session" : "2021-05-26 00:00:00",
		"number_of_slots" : 4,
		"create_date" : "2021-05-20 21:00:00"
	},
	{
		"room_id" : 2,
		"description" : "Podręcznikowa przygoda dla 2 graczy. Uprzedzam - to moja pierwsza przygoda jako Mistrz Gry!",
		"status" : "off",
		"avatar" : null,
		"RoomChat_chat_id" : 2,
		"Users_user_id" : 2,
		"owner_id" : 2,
		"name" : "Alien kontra kolonia",
		"next_session" : "2021-05-27 17:00:00",
		"number_of_slots" : 2,
		"create_date" : "2021-05-24 00:00:00"
	},
	{
		"room_id" : 3,
		"description" : "Hej hej drodzy gracze! Zapraszam na one-shot w świecie cyberpunka!",
		"status" : "off",
		"avatar" : null,
		"RoomChat_chat_id" : 3,
		"Users_user_id" : 3,
		"owner_id" : 3,
		"name" : "Strzelanina za rogiem",
		"next_session" : "2021-05-30 21:30:00",
		"number_of_slots" : 9,
		"create_date" : "2021-05-24 22:00:00"
	},
	{
		"room_id" : 4,
		"description" : "Przygoda autorska przeznaczona dla 4 graczy w świecie Dungeons and Dragons. Prowadzona raczej na śmiesznie.",
		"status" : null,
		"avatar" : null,
		"RoomChat_chat_id" : 4,
		"Users_user_id" : 4,
		"owner_id" : 4,
		"name" : "Jak obrabować smoka",
		"next_session" : "2021-05-30 21:30:00",
		"number_of_slots" : 4,
		"create_date" : "2021-05-20 21:30:00"
	}
]
;

export function configureFakeBackend() {
    let realFetch = window.fetch;
    window.fetch = function (url, opts) {
        return new Promise((resolve, reject) => {
            // wrap in timeout to simulate server api call
            setTimeout(handleRoute, 500);

            function handleRoute() {
                const { method } = opts;
                switch (true) {
                    case url.endsWith('/accounts/authenticate') && method === 'POST':
                        return authenticate();
                    case url.endsWith('/accounts/refresh-token') && method === 'POST':
                        return refreshToken();
                    case url.endsWith('/accounts/revoke-token') && method === 'POST':
                        return revokeToken();
                    case url.endsWith('/accounts/register') && method === 'POST':
                        return register();
                    case url.endsWith('/accounts/verify-email') && method === 'POST':
                        return verifyEmail();
                    case url.endsWith('/accounts/forgot-password') && method === 'POST':
                        return forgotPassword();
                    case url.endsWith('/accounts/validate-reset-token') && method === 'POST':
                        return validateResetToken();
                    case url.endsWith('/accounts/reset-password') && method === 'POST':
                        return resetPassword();
                    case url.endsWith('/accounts') && method === 'GET':
                        return getUsers();
                    case url.match(/\/accounts\/\d+$/) && method === 'GET':
                        return getUserById();
                    case url.endsWith('/accounts') && method === 'POST':
                        return createUser();
                    case url.match(/\/accounts\/\d+$/) && method === 'PUT':
                        return updateUser();
                    case url.match(/\/accounts\/\d+$/) && method === 'DELETE':
                        return deleteUser();
                    case url.endsWith('/tables') && method === 'GET':
                        return getTables();
                    case url.match(/\/tables\/\d+$/) && method === 'GET':
                        return getTableById();
                    case url.endsWith('/tables') && method === 'POST':
                        return createTable();
                    case url.match(/\/tables\/\d+$/) && method === 'PUT':
                        return updateTable();
                    case url.match(/\/tables\/\d+$/) && method === 'DELETE':
                        return deleteTable();
                    default:
                        // pass through any requests not handled above
                        return realFetch(url, opts)
                            .then(response => resolve(response))
                            .catch(error => reject(error));
                }
            }

            // route functions

            function authenticate(props) {
                const email = body().email || props.email;
                const password = body().password || props.password;

                const user = users.find(x => x.email === email && x.password === password && x.isVerified);

                if (!user) return error('Email lub hasło niewłaściwe');

                // add refresh token to user
                user.refreshTokens.push(generateRefreshToken());
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok({
                    id: user.id,
                    email: user.email,
                    title: user.title,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    role: user.role,
                    jwtToken: generateJwtToken(user),
                    age: user.age,
                    city: user.city,
                    description: user.description,
                    avatar: user.avatar,
                    tags: user.tags
                });
            }

            function refreshToken() {
                const refreshToken = getRefreshToken();
                
                if (!refreshToken) return unauthorized();

                const user = users.find(x => x.refreshTokens.includes(refreshToken));
                
                if (!user) return unauthorized();

                // replace old refresh token with a new one and save
                user.refreshTokens = user.refreshTokens.filter(x => x !== refreshToken);
                user.refreshTokens.push(generateRefreshToken());
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok({
                    id: user.id,
                    email: user.email,
                    title: user.title,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    role: user.role,
                    jwtToken: generateJwtToken(user),
                    age: user.age,
                    city: user.city,
                    description: user.description,
                    avatar: user.avatar,
                    tags: user.tags
                })
            }

            function revokeToken() {
                if (!isAuthenticated()) return unauthorized();
                
                const refreshToken = getRefreshToken();
                const user = users.find(x => x.refreshTokens.includes(refreshToken));
                
                // revoke token and save
                user.refreshTokens = user.refreshTokens.filter(x => x !== refreshToken);
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok();
            }

            function register() {
                const user = body();
    
                if (users.find(x => x.email === user.email)) {
                    // display email already registered "email" in alert
                    setTimeout(() => {
                        alertService.info(`
                            <h4>Email Already Registered</h4>
                            <p>Your email ${user.email} is already registered.</p>
                            <p>If you don't know your password please visit the <a href="${location.origin}/account/forgot-password">forgot password</a> page.</p>
                            <div><strong>NOTE:</strong> The fake backend displayed this "email" so you can test without an api. A real backend would send a real email.</div>
                        `, { autoClose: false });
                    }, 1000);

                    // always return ok() response to prevent email enumeration
                    return ok();
                }
    
                // assign user id and a few other properties then save
                user.id = newUserId();
                if (user.id === 1) {
                    // first registered user is an admin
                    user.role = Role.Admin;
                } else {
                    user.role = Role.User;
                }
                user.dateCreated = new Date().toISOString();
                user.verificationToken = new Date().getTime().toString();
                user.isVerified = false;
                user.refreshTokens = [];
                delete user.confirmPassword;
                users.push(user);
                localStorage.setItem(usersKey, JSON.stringify(users));

                // display verification email in alert
                setTimeout(() => {
                    const verifyUrl = `${location.origin}/account/verify-email?token=${user.verificationToken}&password=${user.password}&email=${user.email}`;
                    alertService.info(`
                        <h4>Verification Email</h4>
                        <p>Thanks for registering!</p>
                        <p>Please click the below link to verify your email address:</p>
                        <p><a href="${verifyUrl}">${verifyUrl}</a></p>
                        <div><strong>NOTE:</strong> The fake backend displayed this "email" so you can test without an api. A real backend would send a real email.</div>
                    `, { autoClose: false });
                }, 1000);

                return ok();
            }
    
            function verifyEmail() {
                const { token, email, password } = body();
                console.log(users, token)
                const user = users.find(x => !!x.verificationToken && x.verificationToken === token);
                
                if (!user) return error('Verification failed');
                
                // set is verified flag to true if token is valid
                user.isVerified = true;
                localStorage.setItem(usersKey, JSON.stringify(users));
                return ok(authenticate({email, password}));
            }

            function forgotPassword() {
                const { email } = body();
                const user = users.find(x => x.email === email);
                
                // always return ok() response to prevent email enumeration
                if (!user) return ok();
                
                // create reset token that expires after 24 hours
                user.resetToken = new Date().getTime().toString();
                user.resetTokenExpires = new Date(Date.now() + 24*60*60*1000).toISOString();
                localStorage.setItem(usersKey, JSON.stringify(users));

                // display password reset email in alert
                setTimeout(() => {
                    const resetUrl = `${location.origin}/account/reset-password?token=${user.resetToken}`;
                    alertService.info(`
                        <h4>Reset Password Email</h4>
                        <p>Please click the below link to reset your password, the link will be valid for 1 day:</p>
                        <p><a href="${resetUrl}">${resetUrl}</a></p>
                        <div><strong>NOTE:</strong> The fake backend displayed this "email" so you can test without an api. A real backend would send a real email.</div>
                    `, { autoClose: false });
                }, 1000);

                return ok();
            }

            function validateResetToken() {
                const { token } = body();
                const user = users.find(x =>
                    !!x.resetToken && x.resetToken === token &&
                    new Date() < new Date(x.resetTokenExpires)
                );
                
                if (!user) return error('Invalid token');
                
                return ok();
            }

            function resetPassword() {
                const { token, password } = body();
                const user = users.find(x =>
                    !!x.resetToken && x.resetToken === token &&
                    new Date() < new Date(x.resetTokenExpires)
                );
                
                if (!user) return error('Invalid token');
                
                // update password and remove reset token
                user.password = password;
                user.isVerified = true;
                delete user.resetToken;
                delete user.resetTokenExpires;
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok();
            }

            function getUsers() {
                if (!isAuthorized(Role.Admin)) return unauthorized();

                return ok(users);
            }

            function getTables() {
                return ok(tables);
            }

            function getUserById() {
                if (!isAuthenticated()) return unauthorized();
    
                let user = users.find(x => x.id === idFromUrl());

                // users can get own profile and admins can get all profiles
                if (user.id !== currentUser().id && !isAuthorized(Role.Admin)) {
                    return unauthorized();
                }

                return ok(user);
            }

            function getTableById() {
                let table = table.find(x => x.id === idFromUrl());

                return ok(table);
            }
    
            function createUser() {
                if (!isAuthorized(Role.Admin)) return unauthorized();
    
                const user = body();
                if (users.find(x => x.email === user.email)) {
                    return error(`Email ${user.email} is already registered`);
                }

                // assign user id and a few other properties then save
                user.id = newUserId();
                user.dateCreated = new Date().toISOString();
                user.isVerified = true;
                user.refreshTokens = [];
                delete user.confirmPassword;
                users.push(user);
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok();
            }

            function createTable() {
                const table = body();
                if (tables.find(x => x.name === table.name)) {
                    return error(`Table with this name already exist`);
                }

                // assign user id and a few other properties then save
                table.id = newTableId();
                tables.push(table);
                localStorage.setItem(tablesKey, JSON.stringify(tables));

                return ok();
            }
    
            function updateUser() {
                if (!isAuthenticated()) return unauthorized();
    
                let params = body();
                let user = users.find(x => x.id === idFromUrl());

                // users can update own profile and admins can update all profiles
                if (user.id !== currentUser().id && !isAuthorized(Role.Admin)) {
                    return unauthorized();
                }

                // only update password if included
                if (!params.password) {
                    delete params.password;
                }
                // don't save confirm password
                delete params.confirmPassword;

                // update and save user
                Object.assign(user, params);
                localStorage.setItem(usersKey, JSON.stringify(users));

                return ok({
                    id: user.id,
                    email: user.email,
                    title: user.title,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    role: user.role,
                    age: user.age,
                    city: user.city,
                    description: user.description
                });
            }

            function updateTable() {
                if (!isAuthenticated()) return unauthorized();
    
                let params = body();
                let table = tables.find(x => x.id === idFromUrl());

                // update and save user
                Object.assign(table, params);
                localStorage.setItem(tablesKey, JSON.stringify(tables));

                return ok({
                    id: table.id,
                    name: table.name
                });
            }
    
            function deleteUser() {
                if (!isAuthenticated()) return unauthorized();
    
                let user = users.find(x => x.id === idFromUrl());

                // users can delete own account and admins can delete any account
                if (user.id !== currentUser().id && !isAuthorized(Role.Admin)) {
                    return unauthorized();
                }

                // delete user then save
                users = users.filter(x => x.id !== idFromUrl());
                localStorage.setItem(usersKey, JSON.stringify(users));
                return ok();
            }
    
            function deleteTable() {
                if (!isAuthenticated()) return unauthorized();
    
                let table = tables.find(x => x.id === idFromUrl());

                // delete user then save
                tables = tables.filter(x => x.id !== idFromUrl());
                localStorage.setItem(tablesKey, JSON.stringify(tables));
                return ok();
            }
    
            // helper functions

            function ok(body) {
                resolve({ ok: true, text: () => Promise.resolve(JSON.stringify(body)) });
            }

            function unauthorized() {
                resolve({ status: 401, text: () => Promise.resolve(JSON.stringify({ message: 'Unauthorized' })) });
            }

            function error(message) {
                resolve({ status: 400, text: () => Promise.resolve(JSON.stringify({ message })) });
            }

            function isAuthenticated() {
                return !!currentUser();
            }
    
            function isAuthorized(role) {
                const user = currentUser();
                if (!user) return false;
                return user.role === role;
            }
    
            function idFromUrl() {
                const urlParts = url.split('/');
                return parseInt(urlParts[urlParts.length - 1]);
            }

            function body() {
                return opts.body && JSON.parse(opts.body);    
            }

            function newUserId() {
                return users.length ? Math.max(...users.map(x => x.id)) + 1 : 1;
            }

            function generateJwtToken(user) {
                // create token that expires in 15 minutes
                const tokenPayload = { 
                    exp: Math.round(new Date(Date.now() + 15*60*1000).getTime() / 1000),
                    id: user.id
                }
                return `fake-jwt-token.${btoa(JSON.stringify(tokenPayload))}`;
            }

            function currentUser() {
                // check if jwt token is in auth header
                const authHeader = opts.headers['Authorization'] || '';
                if (!authHeader.startsWith('Bearer fake-jwt-token')) return;

                // check if token is expired
                const jwtToken = JSON.parse(atob(authHeader.split('.')[1]));
                const tokenExpired = Date.now() > (jwtToken.exp * 1000);
                if (tokenExpired) return;

                const user = users.find(x => x.id === jwtToken.id);
                return user;
            }

            function generateRefreshToken() {
                const token = new Date().getTime().toString();

                // add token cookie that expires in 7 days
                const expires = new Date(Date.now() + 7*24*60*60*1000).toUTCString();
                document.cookie = `fakeRefreshToken=${token}; expires=${expires}; path=/`;

                return token;
            }

            function getRefreshToken() {
                // get refresh token from cookie
                return (document.cookie.split(';').find(x => x.includes('fakeRefreshToken')) || '=').split('=')[1];
            }
        });
    }
}