import React from 'react';
import { Link, useHistory  } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import Button from '@material-ui/core/Button';

import { accountService, tableService, alertService } from '@/_services';
import Grid from '@material-ui/core/Grid';
import * as Yup from 'yup';
import TextField from '@material-ui/core/TextField';
import './tableStyles.less';

function CreateTable({ match }) {
    const { path } = match;
    const user = accountService.userValue;
    console.log(user)
    const history = useHistory();

    const initialValues = {
        name: '',
        description: '',
        number_of_slots: 1,
        owner_id: user.user_id
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Nazwa jest wymagana'),
        number_of_slots: Yup.number()
            .min(1, "Musi być co najmniej jedna osoba w pokoju")
            .max(9, "Musi być do 9 osób w pokoju")
            .required('Musisz podać liczbę miejsc w pokoju')            
    });

    function onSubmit(fields) {
        tableService.create(fields)
            .then(() => {
                alertService.success('Udało Ci się dodać pokój!', { keepAfterRouteChange: true });
                history.push("/home");
            })
            .catch(error => {
                alertService.error(error);
            });
    }
    return (
        <div className="create-table-container">
             <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                {({ errors, touched }) => (
                    <Form>
                            <div className="form-row">
                                <div className="form-group col">
                                    <label>Nazwa</label>
                                    <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
                                    <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Opis</label>
                                <Field name="description" type="textarea" className='form-control' />
                            </div>
                            <div className="form-group">
                                <label>Liczba miejsc w pokoju</label>
                                <Field name="number_of_slots" type="number" className={'form-control' + (errors.number_of_slots && touched.number_of_slots ? ' is-invalid' : '')}/>
                                <ErrorMessage name="number_of_slots" component="div" className="invalid-feedback" />
                            </div>
                            
                            <div className="form-group">
                                <Grid container spacing={3} justify="space-between">
                                    <Grid item xs={12} md={6}>
                                        <Button type="submit" variant="contained" color="primary">
                                            Stwórz pokój
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                    </Form>
                )}
            </Formik>
        </div>
    );
}

export { CreateTable };