import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { CreateTable } from './CreateTable';
import { ViewTable } from './ViewTable';
import './tableStyles.less';

const Table = ({ match }) => {
    const { path } = match;
    return (
        <div className="room-component">
             <Switch>
                <Route exact path={`${path}/create`} component={CreateTable} />
                <Route path={`${path}/:room_id`} component={ViewTable} />
                    {/* <Route exact path={path} component={Details} />
                    <Route path={`${path}/update`} component={Update} /> */}
            </Switch>
        </div>
    );
}

export { Table };