import React, { useState, useEffect } from 'react';
import { Link, useHistory, Redirect, useParams  } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import Button from '@material-ui/core/Button';

import { accountService, tableService, alertService } from '@/_services';
import Grid from '@material-ui/core/Grid';
import * as Yup from 'yup';
import TextField from '@material-ui/core/TextField';
import './tableStyles.less';
import { Room } from '@material-ui/icons';
import moment from 'moment';
import ForumIcon from '@material-ui/icons/Forum';
import AddIcon from '@material-ui/icons/Add';
import './tableStyles.less';

const Friend = ({user}) => {
    if(!user.hasOwnProperty("avatar")){
        const randomNumber = Math.floor(Math.random() * 6) + 1;
        user.avatar = `${randomNumber}-avatar`;
    }
    const imgAdress = `src\\resources\\${user.avatar}.jpeg`;

    return (
        <div>
            <Grid container justify="space-between" alignItems="center" spacing={5}>
                <Grid item sm={3}>
                    <img src={imgAdress} className="friend-avatar"></img>
                </Grid>
                <Grid item sm={6}>
                    <h5>@{user.name}</h5>
                </Grid>
                <Grid item sm={3}>
                    <ForumIcon></ForumIcon>
                </Grid>
            </Grid>
        </div>
    )
}
const Friends = ({friends}) => {

    return (
        <div className="friends-container">
            <Grid container justify="space-between" alignItems="center">
                <Grid item sm={8}>
                    <h2>Znajomi</h2>
                </Grid>
                <Grid item sm={4}>
                    <h6><AddIcon></AddIcon>DODAJ</h6>
                </Grid>
            </Grid>
            {friends?.map(function(friend, index){
                    return <Friend key={ index } user={friend}></Friend>;
                  })}
        </div>
    )
}

function ViewTable({ match }) {
    const { path } = match;
    let { room_id } = useParams();
    if (!room_id){
        return (<Redirect to={{ pathname: '/home'}} />)
    }
    const user = accountService.userValue;
    const [room, setRoom] = useState([]);
    const friends = [
        {user_id: 1, name: "ktoś-nowy", avatar: "1-avatar"},
        {user_id: 2, name: "alien_love", avatar: "2-avatar"},
        {user_id: 3, name: "zena", avatar: "3-avatar"},
        {user_id: 4, name: "alien-alien", avatar: "4-avatar"},
    ]

    useEffect(() => {
        tableService.getById(room_id).then(x => setRoom(x[0]));
    }, []);

    const joinRoom = () => {
        const payload = {
            user_id: user.user_id,
            room_id: room_id
        }
        tableService.joinRoom(payload)
            .then(() => {
                alertService.success('Dołączono do pokoju');
                let tempRoom = {...room};
                tempRoom.users.push({user_id: user.user_id, name: user.name})
                setRoom(tempRoom);
            })
            .catch(error => {
                alertService.error(error);
            });
    }

    const leaveRoom = () => {
        const payload = {
            user_id: user.user_id,
            room_id: room_id
        }
        tableService.leaveRoom(payload)
            .then(() => {
                alertService.success('Opuszczono pokój');
                let tempRoom = {...room};
                tempRoom.users = tempRoom.users.filter(member => member.user_id != user.user_id);
                setRoom(tempRoom);
            })
            .catch(error => {
                alertService.error(error);
            });
    }


    if(!room){
        return (
            <div className="view-table-container">
                Nie ma takiego pokoju
            </div> 
        )
    }
    console.log(room)
    return (
        <div className="view-table-container">
              <Grid container spacing={2} justify="center">
                <Grid item md={8}>
                    <div className="view-table-header">
                        <div className="view-table-header-title">
                            <span><img src="src\resources\1-avatar.jpeg" className="friend-avatar"></img> Otworzony przez: {room.users?.find(user => user.user_id == room.owner_id).name}</span>
                            <h3>{room.name}</h3>
                        </div>
                        {room.users?.find(member => user.user_id == member.user_id)?
                        <Button className="view-table-button" variant="contained" onClick={() => leaveRoom()} color="secondary">Opuść pokój</Button>   
                        :
                        <Button className="view-table-button" variant="contained" onClick={() => joinRoom()} color="primary">Dołącz do pokoju</Button>
                        }
                    </div>
                    <div className="view-table-content">
                        <div className="view-table-settings">
                            <h4>Ustawienia pokoju</h4>
                            {(room.hasOwnProperty('tags') && room.tags.length)?
                                <p>{room.tags.map( tag => `#${tag.name} `)}</p>
                            :
                                <p>Nie ma tagów</p>
                            }
                            <p>Liczba miejsc: {room.users?.length}/{room.number_of_slots}</p>
                            
                            {room.next_session?
                                <p>Najbliższe spotkanie {moment(room.next_session).fromNow()}</p>
                            :
                                <p>Nie ma jeszcze zaplanowanego spotkania</p>
                            }
                            <p>Planowana ilość sesji:</p>
                            <p>Preferowany wiek graczy:</p>
                            <h4>Lista członków pokoju:</h4>
                            {room.users?.map(function(member, index){
                                return <Friend key={ index } user={member}></Friend>;
                            })}
                        </div>
                        <div className="view-table-description">
                            <p>{room.description}</p>
                        </div>
                    </div>

                </Grid>
                <Grid item md={4}>
                    <div className="side-panel-container">
                        <Friends friends={friends}></Friends>
                    </div>
                </Grid>
            </Grid>
        </div>
    );
}

export { ViewTable };