import React from 'react';
import { Link } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';

function ForgotPassword() {
    const initialValues = {
        email: ''
    };

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email('Email jest niewłaściwy')
            .required('Email jest wymagany')
    });

    function onSubmit({ email }, { setSubmitting }) {
        alertService.clear();
        accountService.forgotPassword(email)
            .then(() => alertService.success('Sprawdź swój e-mail, żeby zobaczyć dalsze instrukcje'))
            .catch(error => alertService.error(error))
            .finally(() => setSubmitting(false));
    }

    return (
        <div>
            <h2>Resetowanie hasła</h2>
            <h1 className="logo">MeetRPG</h1>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                {({ errors, touched, isSubmitting }) => (
                    <Form>
                        <p>Podaj email na który wyślemy link do zresetowania hasła.</p>
                        <div className="form-group">
                            <label>Email</label>
                            <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                            <ErrorMessage name="email" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-row">
                            <div className="form-group col">
                                <Link to="login" className="btn btn-link">Powrót</Link>
                                <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                    {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                    Wyślij link
                                </button>
                            </div>
                        </div>
                        <div className="form-row">
                            <div className="form-group text-right">
                                <p>Nie masz konta? <Link to="register" className="btn btn-link">Zarejestruj się</Link></p>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>       
    )
}

export { ForgotPassword }; 