import React, { useState, useEffect } from 'react';

import { AccountSettingsForm } from './components/AccountSettingsForm';
import { AvatarForm } from './components/AvatarForm';
import { TagForm } from './components/TagForm';

function AccountSettings({ history, user }) {
    const [path, setPath] = useState('settings')
    
    useEffect(() => {
        if (!user) {
            history.push('/');
        }
        console.log(user);
        if (user?.age && !user?.avatar) {
            setPath('avatar')
        } else if (user?.avatar) {
            setPath('tags')
        };
    }, [user]);

    return (
       <div>
           {path === 'settings' && <AccountSettingsForm user={user} />}
           {path === 'avatar' && <AvatarForm user={user} />}
           {path === 'tags' && <TagForm user={user} history={history} />}
       </div>
    )
}

export { AccountSettings }; 