import React from 'react';
import { Formik, Form } from 'formik';

import { accountService, alertService } from '@/_services';

function AvatarForm({ user }) {
    const initialValues = {
        avatar: ''
    };

    function onSubmit(fields, { setStatus, setSubmitting }) {
        setStatus();
        accountService.update(user.id, fields)
            .then(() => {
                alertService.success('Registration successful, please check your email for verification instructions');
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit}>
            {({ isSubmitting, setFieldValue }) => (
                <Form>
                    <h3 className="card-header">Account Settings</h3>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group">
                                <label>Avatar</label>
                                <input id="avatar" name="avatar" type="file" onChange={(event) => {
                                    setFieldValue("avatar", event.currentTarget.files[0]);
                                }} />
                            </div>
                        </div>
                        <div className="form-group">
                            <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                Dalej
                            </button>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export { AvatarForm }; 