import React from 'react';
import { Formik, Form } from 'formik';

import { accountService, alertService } from '@/_services';

function TagForm({ user, history }) {
    const initialValues = {
        avatar: ''
    };

    function onSubmit(fields, { setStatus, setSubmitting }) {
        const tags = ['dnd', 'fantasy', 'rpg']
        setStatus();
        accountService.update(user.id, {tags})
            .then(() => {
                alertService.success('Wooohooo', { keepAfterRouteChange: true });
                history.push('/home')
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <Formik initialValues={initialValues} onSubmit={onSubmit}>
            {({ isSubmitting }) => (
                <Form>
                    <h3 className="card-header">Account Settings</h3>
                    <div className="card-body">
                        <div className="form-row">
                            <div className="form-group">
                                <label>Tags</label>
                            </div>
                        </div>
                        <div className="form-group">
                            <button type="submit" disabled={isSubmitting} className="btn btn-primary">
                                {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                Dalej
                            </button>
                        </div>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

export { TagForm }; 