import React, { useState } from 'react';
import { Link, useHistory  } from 'react-router-dom';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import * as Yup from 'yup';
import md5 from 'md5';

import { accountService, alertService } from '@/_services';

import "./AccountStyles.less";

function Register() {
    const history = useHistory();

    const initialValues = {
        name: '',
        email: '',
        password: '',
        confirmPassword: '',
        acceptTerms: false,
        isAdult: false
    };

    const validationSchema = Yup.object().shape({
        name: Yup.string()
            .required('Nazwa jest wymagana'),
        email: Yup.string()
            .email('Email niepoprawny')
            .required('Email jest wymagany'),
        password: Yup.string()
            .matches(
                /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                "Musi zawierać 8 znaków, jedną wielką literę, jedną małą, jedną cyfrę i jeden znak specjalny"
              )
            .required('Hasło wymagane'),
        confirmPassword: Yup.string()
            .oneOf([Yup.ref('password'), null], 'Hasła muszą być takie same')
            .required('Potwierdzenie hasła wymagane'),
        acceptTerms: Yup.bool()
            .oneOf([true], 'Zaakceptowanie Regulaminu i Polityki prywatności wymagane'),
        isAdult: Yup.bool()
            .oneOf([true], 'Trzeba być osobą pełnoletnią lub mieć zgodę na korzystanie z serwisu')
    });

    function onSubmit(fields, { setSubmitting }) {
        setSubmitting(true)
        fields.password = md5(fields.password);
        accountService.register(fields)
            .then(() => {
                alertService.success('Rejestracja udała się, teraz możesz się zalogować', { keepAfterRouteChange: true });
                history.push("/account/login");
                setSubmitting(false);
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <div>
            <h2>Zarejestruj się do</h2>
            <h1 className="logo">MeetRPG</h1>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                {({ errors, touched, isSubmitting }) => (
                    <Form>
                            <div className="form-row">
                                <div className="form-group col">
                                    <label>Nazwa</label>
                                    <Field name="name" type="text" className={'form-control' + (errors.name && touched.name ? ' is-invalid' : '')} />
                                    <ErrorMessage name="name" component="div" className="invalid-feedback" />
                                </div>
                            </div>
                            <div className="form-group">
                                <label>Email</label>
                                <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                                <ErrorMessage name="email" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <label>Hasło</label>
                                <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                                <ErrorMessage name="password" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <label>Potwierdź hasło</label>
                                <Field name="confirmPassword" type="password" className={'form-control' + (errors.confirmPassword && touched.confirmPassword ? ' is-invalid' : '')} />
                                <ErrorMessage name="confirmPassword" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group form-check">
                                <Field type="checkbox" name="acceptTerms" id="acceptTerms" className={'form-check-input ' + (errors.acceptTerms && touched.acceptTerms ? ' is-invalid' : '')} />
                                <label htmlFor="acceptTerms" className="form-check-label">Zaakceptuj Regulamin oraz Politykę prywatności</label>
                                <ErrorMessage name="acceptTerms" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group form-check">
                                <Field type="checkbox" name="isAdult" id="isAdult" className={'form-check-input ' + (errors.isAdult && touched.isAdult ? ' is-invalid' : '')} />
                                <label htmlFor="isAdult" className="form-check-label">Jestem osobą pełnoletnią lub otrzymałem zgodę opiekuna na korzystanie z serwisu.</label>
                                <ErrorMessage name="isAdult" component="div" className="invalid-feedback" />
                            </div>
                            <div className="form-group">
                                <Grid container spacing={3} justify="space-between">
                                    <Grid item xs={12} md={6}>
                                        <Link to="/" ><Button className="special-button account-button" variant="contained">Powrót</Button></Link>
                                    </Grid>
                                    <Grid item xs={12} md={6}>
                                        <Button type="submit" disabled={isSubmitting} className="account-button" variant="contained" color="primary">
                                            {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                            Zarejestruj
                                        </Button>
                                    </Grid>
                                </Grid>
                            </div>
                            <p>
                                Masz już konto?<Link to="login" className="btn btn-link">Zaloguj się</Link>
                            </p>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export { Register }; 