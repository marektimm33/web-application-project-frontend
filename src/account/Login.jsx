import React from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { accountService, alertService } from '@/_services';

function Login({ history, location }) {
    const initialValues = {
        email: '',
        password: ''
    };

    const validationSchema = Yup.object().shape({
        email: Yup.string()
            .email('Email jest niewłaściwy')
            .required('Email jest wymagany'),
        password: Yup.string().required('Hasło jest wymagane')
    });

    function onSubmit({ email, password }, { setSubmitting }) {
        alertService.clear();
        accountService.login(email, password)
            .then((response) => {
                const { from } = location.state || { from: { pathname: "/home" } };
                history.push(from);
            })
            .catch(error => {
                setSubmitting(false);
                alertService.error(error);
            });
    }

    return (
        <div>
            <h2>Zaloguj się do</h2>
            <h1 className="logo">MeetRPG</h1>
            <Formik initialValues={initialValues} validationSchema={validationSchema} onSubmit={onSubmit}>
                {({ errors, touched, isSubmitting }) => (
                    <Form>
                        <div className="form-group">
                            <label>Email</label>
                            <Field name="email" type="text" className={'form-control' + (errors.email && touched.email ? ' is-invalid' : '')} />
                            <ErrorMessage name="email" component="div" className="invalid-feedback" />
                        </div>
                        <div className="form-group">
                            <label>Hasło</label>
                            <Field name="password" type="password" className={'form-control' + (errors.password && touched.password ? ' is-invalid' : '')} />
                            <ErrorMessage name="password" component="div" className="invalid-feedback" />
                        </div>
                        {/* <div className="form-row">
                            <div className="form-group col text-right">
                                <Link to="forgot-password" className="btn btn-link pr-0">Zapomniałem hasła</Link>
                            </div>
                        </div> */}
                        <div className="form-row">
                            <Grid container spacing={3} justify="space-between">
                                <Grid item xs={12} md={6}>
                                    <Link to="/" ><Button className="special-button account-button" variant="contained">Powrót</Button></Link>
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <Button type="submit" disabled={isSubmitting} className="account-button" variant="contained" color="primary">
                                        {isSubmitting && <span className="spinner-border spinner-border-sm mr-1"></span>}
                                        Zaloguj
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                        <div className="form-row">
                            <div className="form-group text-right">
                                <p>Nie masz konta? <Link to="register" className="btn btn-link">Zarejestruj się</Link></p>
                            </div>
                        </div>
                    </Form>
                )}
            </Formik>
        </div>
    )
}

export { Login }; 