import React, { useEffect, useState } from 'react';
import { Route, Switch } from 'react-router-dom';

import { accountService } from '@/_services';

import { Login } from './Login';
import { Register } from './Register';
import { AccountSettings } from './AccountSettings';
import { VerifyEmail } from './VerifyEmail';
import { ForgotPassword } from './ForgotPassword';
import { ResetPassword } from './ResetPassword';

import "./AccountStyles.less";
function Account({ history, match, user }) {
    const { path } = match;

    return (
        <div className="account-wrapper">
            <div className="account-block">
                <Switch>
                    <Route path={`${path}/login`} component={Login} />
                    <Route path={`${path}/register`} component={Register} />
                    <Route path={`${path}/account-settings`} component={(props) => <AccountSettings user={user} {...props} />} />
                    <Route path={`${path}/verify-email`} component={VerifyEmail} />
                    <Route path={`${path}/forgot-password`} component={ForgotPassword} />
                    <Route path={`${path}/reset-password`} component={ResetPassword} />
                </Switch>
            </div>
        </div>
    );
}

export { Account };