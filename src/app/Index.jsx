import React, { useState, useEffect } from 'react';
import { Route, Switch, Redirect, useLocation } from 'react-router-dom';

import { Role, history } from '@/_helpers';
import { accountService } from '@/_services';
import { Nav, PrivateRoute, Alert, Footer } from '@/_components';
import { Home } from '@/home';
import { Profile } from '@/profile';
import { Table } from '@/table';
import { Chat } from '@/chat';
import { Admin } from '@/admin';
import { Account } from '@/account';
import { LandingPage } from '@/landing';

function App() {
    const { pathname } = useLocation();  
    const [user, setUser] = useState({});

    useEffect(() => {
        const subscription = accountService.user.subscribe(x => setUser(x));
        return subscription.unsubscribe;
    }, []);

    return (
        <div className={'app-container' + (user && ' bg-light')}>
            <div className="allButFooter">
                <Nav />
                <Alert />
                <Switch>
                    <Redirect from="/:url*(/+)" to={pathname.slice(0, -1)} />
                    <PrivateRoute path="/home" component={Home} />
                    <PrivateRoute path="/profile" component={Profile} />
                    <PrivateRoute path="/table" component={Table} />
                    <PrivateRoute path="/chat" component={Chat} />
                    <PrivateRoute path="/admin" roles={[Role.Admin]} component={Admin} />
                    <Route path="/account" component={(props) => <Account user={user} {...props} />} />
                    <Route exact path="/" component={LandingPage} />
                    <Redirect from="*" to="/" />
                </Switch>
            </div>
            <Footer />
        </div>
    );
}

export { App }; 